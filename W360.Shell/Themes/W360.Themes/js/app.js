﻿/*
    Modules for controlling layout.
    Import (import {function} from "./module.js" ) doesnt work since 
    Web Compiler uses require : require('module name') and not Javascript import.
    Maybe we can add support for it by adding modules by npm.

    TODO: Move this project into npm.
 */



/*
 * Client side form, fields validation.
 */
function formSubmit() {
    var forms = document.getElementsByClassName('form-validation');
    Array.prototype.filter.call(forms, function (form) {
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        form.classList.add('was-validated');
    });
}


export function formValid() {
    var forms = document.getElementsByClassName('form-validation');
    Array.prototype.filter.call(forms, function (form) {
        console.log('Form valid: ', form.checkValidity())

        if (form.checkValidity())
            return 'yes'
        return 'no'
    });
}



/*
 * Toggle sidebar menu
 */
function toggleSidebarMenu() {
    document.getElementById('sidenav').classList.toggle('active');
}

/*
 * Toggle right side theme panel
 */
function toggleThemePanel() {
    document.getElementById('themepanel').classList.toggle('active');
}

/*
 * Hides themepanel if open, when user clicks outside theme panel.
 */
addEventListener('pointerdown', function (event) {
    var panel = document.getElementById('themepanel');
    var target = panel.getBoundingClientRect();

    if (event.clientX >= target.left && event.clientX <= target.right &&
        event.clientY >= target.top && event.clientY <= target.bottom) {    
    } else { panel.classList.remove('active');}
}, false);





/*
 * Display errors from console.error in a div automatically. 
 * Not on mobile devices.
 */
addEventListener('error', (e) => {
    let useconsole = localStorage.getItem('console');
    if (useconsole == null) useLogConsole(true);

    if (useconsole == 'true') {
        var log = document.getElementById('shell');
        var text = document.getElementById('error-log');

        log.style.display = 'block';
        text.innerHTML = `${e.message}\n ${e.error}\n At: ${e.lineno} ${e.colno}\n ${e.source}`;
        setTimeout(function () { log.style.display = 'none'; }, 5000);
    }
});

/*
 * Disable or activate console panel 
 * @param arg: boolean
 */
function useLogConsole(arg) {
    localStorage.setItem('console', arg);
}






/*
* Theme manager
* Save selected style to localStorage.
* Add theme: Add more themes in ./Shared/ThemePanel.razor file
* TODO: Add automatic mapping of themes.
* @param theme: string
*/

let ThemeList = new Map();
ThemeList.set('shell', { link: '/css/app.theme.min.css.gz' });
ThemeList.set('developer', { link: '/css/developer.theme.min.css.gz' });
ThemeList.set('wl360', { link: '/css/wl360.theme.min.css.gz' });

function setTheme(theme) {

    var current = ThemeList.get(theme);
    var csslink = document.getElementById('theme-id');

    if (current != null) {
        csslink.href = current.link;
        localStorage.setItem('theme', theme);
    } else {
        csslink.href = ThemeList.get('shell').link;
        localStorage.setItem('theme', 'shell');
    }

    document.getElementById('themepanel').classList.toggle('active');
}

/*
 * Get the current theme
 * TODO: Add to ProtectedLocalStorage in 360! 
 */
let style = localStorage.getItem('theme');

if (style == null) {
    setTheme('shell');
} else {
    setTheme(style);
}





/*
 * Splitter component
 * Enables a resize handle between two areas.
 */
addEventListener('pointerdown', function (pointerDownEvent) {
    var _a;
    var separator = pointerDownEvent.target;
    var container = separator.parentElement;
    if (!container || !pointerDownEvent.isPrimary || pointerDownEvent.button !== 0 || separator.getAttribute('role') !== 'separator') {
        return;
    }
    var vertical = container.hasAttribute('data-flex-splitter-vertical');
    var horizontal = container.hasAttribute('data-flex-splitter-horizontal');
    if (!vertical && !horizontal) {
        return;
    }
    // prevent text selection when drag.
    pointerDownEvent.preventDefault();
    var pointerId = pointerDownEvent.pointerId;
    var pane1 = separator.previousElementSibling;
    var pane2 = separator.nextElementSibling;
    var containerStyle = getComputedStyle(container);
    if ((containerStyle.flexDirection.indexOf('reverse') !== -1 ? -1 : 1) * (horizontal && containerStyle.direction === 'rtl' ? -1 : 1) === -1) {
        _a = [pane2, pane1], pane1 = _a[0], pane2 = _a[1];
    }
    var pane1ComputedStyle = getComputedStyle(pane1);
    var pane2ComputedStyle = getComputedStyle(pane2);
    var pane1Rect = pane1.getBoundingClientRect();
    var onPointerMove;
    if (vertical) {
        var pane1Pos_1 = pane1Rect.top + pointerDownEvent.offsetY;
        var totalSize_1 = pane1.offsetHeight + pane2.offsetHeight;
        var pane1MinSize_1 = Math.max(parseInt(pane1ComputedStyle.minHeight, 10) || 0, totalSize_1 - (parseInt(pane2ComputedStyle.maxHeight, 10) || totalSize_1));
        var pane1MaxSize_1 = Math.min(parseInt(pane1ComputedStyle.maxHeight, 10) || totalSize_1, totalSize_1 - (parseInt(pane2ComputedStyle.minHeight, 10) || 0));
        onPointerMove = function (event) {
            if (event.pointerId === pointerId) {
                var pane1Size = Math.round(Math.min(Math.max(event.clientY - pane1Pos_1, pane1MinSize_1), pane1MaxSize_1));
                pane1.style.height = pane1Size + 'px';
                pane2.style.height = totalSize_1 - pane1Size + 'px';
            }
        };
    }
    else {
        var pane1Pos_2 = pane1Rect.left + pointerDownEvent.offsetX;
        var totalSize_2 = pane1.offsetWidth + pane2.clientWidth;
        var pane1MinSize_2 = Math.max(parseInt(pane1ComputedStyle.minWidth, 10) || 0, totalSize_2 - (parseInt(pane2ComputedStyle.maxWidth, 10) || totalSize_2));
        var pane1MaxSize_2 = Math.min(parseInt(pane1ComputedStyle.maxWidth, 10) || totalSize_2, totalSize_2 - (parseInt(pane2ComputedStyle.minWidth, 10) || 0));
        onPointerMove = function (event) {
            if (event.pointerId === pointerId) {
                var pane1Size = Math.round(Math.min(Math.max(event.clientX - pane1Pos_2, pane1MinSize_2), pane1MaxSize_2));
                pane1.style.width = pane1Size + 'px';
                pane2.style.width = totalSize_2 - pane1Size + 'px';
            }
        };
    }
    var onPointerUp = function (event) {
        if (event.pointerId === pointerId) {
            separator.releasePointerCapture(pointerId);
            separator.removeEventListener('pointermove', onPointerMove);
            separator.removeEventListener('pointerup', onPointerUp);
            separator.removeEventListener('pointercancel', onPointerUp);
        }
    };
    onPointerMove(pointerDownEvent);
    pane1.style.flexShrink = pane2.style.flexShrink = 1;
    separator.addEventListener('pointercancel', onPointerUp);
    separator.addEventListener('pointerup', onPointerUp);
    separator.addEventListener('pointermove', onPointerMove);
    separator.setPointerCapture(pointerId);
});