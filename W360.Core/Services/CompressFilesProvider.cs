﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using System.IO.Compression;

namespace W360.Shell
{
    public static class CompressFilesProvider
    {
        /// <summary>
        /// Add compress
        /// </summary>
        /// <param name="services"></param>
        public static void AddCompressedFiles(this IServiceCollection services)
        {
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Fastest);
            services.AddResponseCompression(options => {
                options.EnableForHttps = true;
                options.Providers.Add<GzipCompressionProvider>();
                options.MimeTypes = new[] {
                            "text/css",
                            "application/javascript"
                        };
            });
        }

        /// <summary>
        /// Enables compressed files to output response.
        /// </summary>
        /// <param name="app"></param>
        public static void UseCompressedFiles(this IApplicationBuilder app)
        {
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = context =>
                {
                    IHeaderDictionary headers = context.Context.Response.Headers;
                    string contentType = headers["Content-Type"];
                   
                    /*
                     * Add more content types if needed.
                     */
                    if (contentType == "application/x-gzip")
                    {
                        if (context.File.Name.EndsWith("css.gz"))
                        {
                            contentType = "text/css";
                        }
                        if (context.File.Name.EndsWith("js.gz"))
                        {
                            contentType = " text/javascript";
                        }

                        headers.Add("Content-Encoding", "gzip");
                        headers["Content-Type"] = contentType;
                    }
                }
            });
        }
    }
}
