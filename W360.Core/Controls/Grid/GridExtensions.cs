﻿using DevExpress.Blazor;

namespace W360.Shell
{
    public static class GridExtensions
    {
        /// <summary>
        /// Adds an row index to grid rows.
        /// </summary>
        /// <typeparam name="T">Type in grid</typeparam>
        /// <param name="row">DataGridHtmlDataCellDecorationEventArgs<T> row</param>
        /// <param name="grid">Grid<T> reference</param>
        public static void AddRowIndex<T>(this DataGridHtmlDataCellDecorationEventArgs<T> cell, DxDataGrid<T> grid)
        {
            cell.Attributes.Add("data-index", grid.PageIndex * grid.PageSize + cell.RowVisibleIndex + 1);
            cell.CssClass = "dx-row-index";
        }

        /// <summary>
        /// Set text to bold for the fieldname cell.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <param name="fieldName"></param>
        public static void SetCellTextBold<T>(this DataGridHtmlDataCellDecorationEventArgs<T> cell, string fieldName)
        {
            cell.CssClass = "dx-cell-highlight";
        }
    }
}
