# W360 WlCom 

## W360.Core
	Contains classes related to W360.Shell

## W360.Shell
	
	Contains web application

## W360.Themes

	Contains builder for css and js files
	Compile Scss and JS files to wwwroot folder

	Usage:
	Install Web Compiler from VS extension menu
	Touch compilerconfig.json and hit ctrl+s, this will 
	create a new fresh compilation of css files.

	Build and create app.js
	Right click Js file and select "Web compiler" -> Re-Compile
	
